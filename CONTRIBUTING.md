# Contributing to Protoclay Engine

The contributors are listed in AUTHORS (add yourself). This project uses the MPL v2 license, see LICENSE.

Please read these documents BEFORE you send a patch:

* Protoclay Engine uses the [C4.2 (Collective Code Construction Contract)](http://rfc.zeromq.org/spec:42) process for contributions. Please read this if you are unfamiliar with it.

* Protoclay Engine uses the [Twitter Java Style Guide ](https://github.com/twitter/commons/blob/master/src/java/com/twitter/common/styleguide.md) for code style. Please read this, and also read the existing code base.

Protoclay Engine grows by the slow and careful accretion of simple, minimal solutions to real problems faced by many people. Some people seem to not understand this. So in case of doubt:

* Each patch defines one clear and agreed problem, and one clear, minimal, plausible solution. If you come with a large, complex problem and a large, complex solution, you will provoke a negative reaction from Protoclay Engine maintainers and users.

* We will usually merge patches aggressively, without a blocking review. If you send us bad patches, without taking the care to read and understand our rules, that reflects on you. Do NOT expect us to do your homework for you.

* As rapidly we will merge poor quality patches, we will remove them again. If you insist on arguing about this and trying to justify your changes, we will simply ignore you and your patches. If you still insist, we will ban you.

* Protoclay Engine is not a sandbox where "anything goes until the next stable release". If you want to experiment, please work in your own projects.