package protoclay.engine.primitives.projectors

import protoclay.engine.domain.*

final class ResizableOpen {
    private static final String uniqueCatalogName = "ResizableOpen"

    def private static final connectorRoleMap = [
            titleSink: ConnectorRole.Sink,
            menuSink : ConnectorRole.Sink,
            frameSink: ConnectorRole.Sink
    ]

    def private static final componentInteriorType = ComponentInterior.Type.Code

    private ResizableOpen() {
        // Disallow construction
    }

    static final ComponentDefinition createComponentDefinition() {
        return new ComponentDefinition(uniqueCatalogName, connectorRoleMap, null) // FIXME Missing argument
    }

    def private static final componentInteriorMap = [
            type: ComponentInterior.Type.Code,
            titleSink: [
                    role           : ConnectorRole.Sink,
                    accept         : { Sink sink, state ->
                        println("accept($sink)")
                    },
                    updateComponent: { Dependent dependent ->
                        println("updateComponent($dependent)")
                    }
            ]
    ]
}
