package protoclay.engine.primitives.sources

import protoclay.engine.domain.ComponentDefinition
import protoclay.engine.domain.ComponentInterior
import protoclay.engine.domain.ConnectorRole
import protoclay.engine.domain.Owner

final class TextSource {
    private static final String uniqueCatalogName = "TextSource"

    def private static final connectorRoleMap = [
            'textSource': ConnectorRole.Source
    ]

    private static final ComponentInterior.Type componentInteriorType = ComponentInterior.Type.Code

    private TextSource() {
        // Disallow construction
    }

    static final ComponentDefinition createComponentDefinition() {
        return new ComponentDefinition(uniqueCatalogName, connectorRoleMap, null) // FIXME Missing argument
    }

    def private static final codeInterior = [
            textSource: [
                    role        : ConnectorRole.Source,
                    processEvent: { Owner owner ->
                        println("processEvent($owner)")
                    }
            ]
    ]
}
