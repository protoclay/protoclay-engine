package protoclay.engine.domain

interface FlowWrapper {

    Owner getOwner()

    List<Dependent> getDependents()
}