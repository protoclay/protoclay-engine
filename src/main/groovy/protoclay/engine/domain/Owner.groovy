package protoclay.engine.domain

class Owner extends Source {

    Owner(WorkingComponent workingComponent, DomainObject domainObject) {
        super(workingComponent, new DataWrapper(domainObject))
    }

    void ownerBeNotified(DataWrapper dataWrapper) {

    }
}
