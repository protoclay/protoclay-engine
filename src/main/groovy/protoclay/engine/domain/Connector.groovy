package protoclay.engine.domain

interface Connector {
    WorkingComponent getWorkingComponent()

    FlowWrapper getFlowWrapper()
}