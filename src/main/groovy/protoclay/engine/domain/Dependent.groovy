package protoclay.engine.domain

class Dependent extends Sink {
    Dependent(WorkingComponent workingComponent) {
        super(workingComponent)
    }

    void dependentBeNotified(DataWrapper dataWrapper) {

    }
}
