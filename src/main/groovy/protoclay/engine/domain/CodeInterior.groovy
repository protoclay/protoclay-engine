package protoclay.engine.domain

class CodeInterior implements ComponentInterior {
    final def sinkFunctionMap = [:]
    final def sourceFunctionMap = [:]

    CodeInterior(sinkFunctionMap, sourceFunctionMap) {
        this.sinkFunctionMap
    }

    private Object mapSinkFunctions(String connectorIdentifier) {
        return [
                'accept'         : { sink -> println("accept($sink)") },
                'updateComponent': { dependent -> println("updateComponent($dependent)") }
        ]
    }

    private Object mapSourceFunctions(String connectorIdentifier) {
        return [
                'processEvent': { owner -> println("processEvent($owner)") }
        ]
    }

    @Override
    void accept(Sink sink) {
        // TODO Implement accept by invoking sinkFunctionMap code
    }

    @Override
    void updateComponent(Dependent dependent) {

    }

    @Override
    void processEvent(Owner owner) {

    }
}
