package protoclay.engine.domain

class EmptyWrapper implements FlowWrapper {
    public static final EmptyWrapper instance = new EmptyWrapper()

    private EmptyWrapper() {
    }

    static boolean equals(FlowWrapper flowWrapper) {
        return flowWrapper == instance
    }

    @Override
    Owner getOwner() {
        // TODO Does EmptyWrapper need to return something else than null from getOwner()?
        return null
    }

    @Override
    List<Dependent> getDependents() {
        return []
    }
}
