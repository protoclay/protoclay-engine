package protoclay.engine.domain

class DataWrapper implements FlowWrapper {
    private final DomainObject domainObject
    private final List<Dependent> dependents

    DataWrapper(DomainObject domainObject) {
        this.domainObject = domainObject
        this.dependents = []
    }

    @Override
    Owner getOwner() {
        return null
    }

    @Override
    List<Dependent> getDependents() {
        return null
    }

    void registerDependent(Dependent dependent) {
        this.dependents.add(dependent)
    }
}
