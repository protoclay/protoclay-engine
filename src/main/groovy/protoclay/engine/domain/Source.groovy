package protoclay.engine.domain

class Source implements Connector {
    private WorkingComponent workingComponent
    private FlowWrapper flowWrapper

    Source(WorkingComponent workingComponent) {
        this(workingComponent, EmptyWrapper.instance)
    }

    Source(WorkingComponent workingComponent, FlowWrapper flowWrapper) {
        this.workingComponent = workingComponent
        this.flowWrapper = flowWrapper
    }

    @Override
    WorkingComponent getWorkingComponent() {
        return this.workingComponent
    }

    @Override
    FlowWrapper getFlowWrapper() {
        return this.flowWrapper
    }

    void forward() {

    }
}
