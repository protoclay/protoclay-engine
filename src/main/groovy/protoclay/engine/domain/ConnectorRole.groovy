package protoclay.engine.domain

enum ConnectorRole {
    Source, Sink, Owner, Dependent
}