package protoclay.engine.domain

class Sink implements Connector {

    private final WorkingComponent workingComponent
    private FlowWrapper flowWrapper

    Sink(WorkingComponent workingComponent) {
        this.workingComponent = workingComponent
        this.flowWrapper = EmptyWrapper.instance
    }

    @Override
    WorkingComponent getWorkingComponent() {
        return this.workingComponent
    }

    @Override
    FlowWrapper getFlowWrapper() {
        return this.flowWrapper
    }

    void forward(FlowWrapper flowWrapper) {
        if (EmptyWrapper.equals(flowWrapper) && EmptyWrapper.equals(this.flowWrapper)) {
            return
        }
        this.flowWrapper = flowWrapper
        this.workingComponent.componentInterior.accept(this)
    }
}
