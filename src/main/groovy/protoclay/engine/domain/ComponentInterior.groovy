package protoclay.engine.domain

interface ComponentInterior {

    enum Type {
        Code, Network
    }

    void accept(Sink sink)

    void updateComponent(Dependent dependent)

    void processEvent(Owner owner)
}