package protoclay.engine.domain

class WorkingComponent {
    final ComponentDefinition componentDefinition
    final Map<String, Connector> connectorMap

    final ComponentInterior componentInterior

    WorkingComponent(ComponentDefinition componentDefinition, Map<String, ConnectorRole> connectorRoleMap, ComponentInterior.Type componentInteriorType) {
        this.componentDefinition = componentDefinition
        this.connectorMap = createConnectorMap(connectorRoleMap)
        this.componentInterior = createComponentInterior(componentInteriorType, this.connectorMap)
    }

    private Map<String, Connector> createConnectorMap(Map<String, ConnectorRole> connectorRoleMap) {
        return connectorRoleMap.collectEntries { key, value ->
            switch (value) {
                case ConnectorRole.Source:
                    return [(key): new Source(this)]
                case ConnectorRole.Owner:
                    return [(key): new Owner(this, null)] // FIXME Missing argument
                case ConnectorRole.Sink:
                    return [(key): new Sink(this)]
                case ConnectorRole.Dependent:
                    return [(key): new Dependent(this)]
                default:
                    throw new IllegalArgumentException("Connector role map contains invalid value for key " + key + ": " + value)
            }
        }.asImmutable()
    }

    ComponentInterior createComponentInterior(ComponentInterior.Type componentInteriorType, Map<String, Connector> connectorMap) {
        switch (componentInteriorType) {
            case ComponentInterior.Type.Code:
                return new CodeInterior(null) // FIXME Missing argument
            case ComponentInterior.Type.Network:
                throw new UnsupportedOperationException("Network interior not supported yet")
            default:
                throw new IllegalStateException("Invalid component interior type: " + this.componentInteriorType)
        }
    }

}
