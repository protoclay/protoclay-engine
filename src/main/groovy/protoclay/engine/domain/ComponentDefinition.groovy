package protoclay.engine.domain

class ComponentDefinition {
    final String uniqueCatalogName
    final Map<String, ConnectorRole> connectorRoleMap
    final ComponentInterior componentInterior

    ComponentDefinition(String uniqueCatalogName, Map<String, ConnectorRole> connectorRoleMap, ComponentInterior componentInterior) {
        this.uniqueCatalogName = uniqueCatalogName
        this.connectorRoleMap = connectorRoleMap.collectEntries { key, value ->
            return [(key): value]
        }.asImmutable()
        this.componentInterior = componentInterior
    }

    WorkingComponent createWorkingComponent() {
        return new WorkingComponent(this, this.connectorRoleMap, null) // FIXME Missing argument
    }
}