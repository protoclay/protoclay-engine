package protoclay.engine.domain

class Connection {
    Source source
    Sink sink

    void send(Source source) {
        sink.forward(source.getFlowWrapper())
    }
}
