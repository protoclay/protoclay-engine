package protoclay.engine.primitives.sources

import protoclay.engine.domain.Source
import spock.lang.Specification

class TextSourceSpec extends Specification {
    def "TextSource component must have 1 source"() {
        given:
        def componentDefinition = TextSource.createComponentDefinition()
        when:
        def workingComponent = componentDefinition.createWorkingComponent()
        then:
        workingComponent.connectorMap.size() == 1
        workingComponent.connectorMap.each { key, value -> value instanceof Source }
    }
}
