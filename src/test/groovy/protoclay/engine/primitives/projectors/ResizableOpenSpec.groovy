package protoclay.engine.primitives.projectors

import protoclay.engine.domain.Sink
import spock.lang.Specification

class ResizableOpenSpec extends Specification {
    def "ResizableOpen component must have 3 sinks"() {
        given:
        def componentDefinition = ResizableOpen.createComponentDefinition()
        when:
        def workingComponent = componentDefinition.createWorkingComponent()
        then:
        workingComponent.connectorMap.size() == 3
        workingComponent.connectorMap.each { key, value -> value instanceof Sink }
    }
}
