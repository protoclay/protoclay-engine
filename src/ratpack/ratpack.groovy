import static ratpack.groovy.Groovy.ratpack

ratpack {
    handlers {
        get {
            render "Protoclay Engine is up and running!"
        }
    }
}