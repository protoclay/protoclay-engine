# Protoclay Engine
Java/Groovy application engine for building applications following the concepts of Mel Conway's #HumanizeTheCraft @mel_conway

[![build status](https://gitlab.com/protoclay/protoclay-engine/badges/master/build.svg)](https://gitlab.com/protoclay/protoclay-engine/commits/master)

**Work in Progress**

## License
[Mozilla Public License version 2.0](https://www.mozilla.org/en-US/MPL/2.0/)

## Links
* [Preact](https://github.com/developit/preact)
* [preact-compat](https://github.com/developit/preact-compat)
* [webpack](https://webpack.github.io)
